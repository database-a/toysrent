from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

from account.functions import roles_required
from toysrent.database import Database
from account.views import _set_empty_field_to_null, _required_fields_is_filled

import copy
import ast
import json

# item
@require_http_methods(["GET"])
@roles_required('any')
def item_index(request, category=None, sort=None):
  # for filter dropdown
  category_query_result = Database.executeThenFetchAll(
    'SELECT name FROM category;'
  )
  category_selection = [e[0] for e in category_query_result]

  # for table
  if sort:
    attribute = None
    order = None
    if sort in [1,2]:
      attribute = 'item_name' 
    if sort in [3,4]:
      attribute = 'category_name' 
    if sort in [1,3]:
      order = 'asc' 
    if sort in [2,4]:
      order = 'desc' 
    additional = 'order by {} {}'.format(attribute, order)
    query_result = Database.executeThenFetchAll(
    'SELECT * FROM item_category ' + additional + ';'
    )
  else:
    query_result = Database.executeThenFetchAll(
      'SELECT * FROM item_category;'
    )
  item_to_categoires = {}
  for item_name, category_name in query_result:
    if not item_name in item_to_categoires.keys():
      item_to_categoires[item_name] = [category_name]
    else:
      item_to_categoires[item_name].append(category_name)
  
  # filter item that only have category in parameter
  if category:
    keys_to_be_deleted = []
    for item, categories in item_to_categoires.items():
      if not category in categories:
        keys_to_be_deleted.append(item)

    for key in keys_to_be_deleted:
      del item_to_categoires[key]

  return render(request, 'toys/item/index.html', 
    {'data': item_to_categoires, 'category_select': category_selection,'category':category,'sort':sort})

@require_http_methods(["GET", "POST"])
@roles_required('admin')
def item_create(request):
  form = { 'error': {} }
  query_result = Database.executeThenFetchAll(
    'SELECT name FROM category;'
  )
  form['category_select'] = [e[0] for e in query_result]

  if request.method == 'GET':
    return render(request, 'toys/item/create.html', {'form': form})
  else:
    form['data'] = copy.deepcopy(request.POST)
    post_data = _set_empty_field_to_null(dict(request.POST))

    # do validation
    if not _required_fields_is_filled(post_data, ['name']):
      form['error']['name'] = 'Name required'
    if post_data['min_age'][0] > post_data['max_age'][0]:
      form['error']['max_age'] = 'Maximum age should be higher than minimum age'
    if form['error']:
      return render(request, 'toys/item/create.html', {'form': form})

    Database.executeThenCommit(
      'INSERT INTO ITEM VALUES(%s, %s, %s, %s, %s);',
      (post_data["name"][0], post_data["description"][0], post_data["min_age"][0],
          post_data["max_age"][0], post_data["material"][0])
    )
    for category in post_data["category"]:
      Database.executeThenCommit(
        'INSERT INTO ITEM_CATEGORY VALUES(%s, %s);',
        (post_data["name"][0], category)
      )

    messages.success(request, 'item created successfully')
    return HttpResponseRedirect(reverse('toys:item_index'))

@require_http_methods(["GET", "POST"])
@roles_required('admin')
def item_update(request, name):
  form = { 'error': {} }
  query_result = Database.executeThenFetchAll(
    'SELECT name FROM category;'
  )
  # get selection for category dropdown
  form['category_select'] = [e[0] for e in query_result]

  if request.method == 'GET':
    item_query_result = Database.executeThenFetchAll(
      'SELECT * FROM item where name=%s;',
      (name, )
    )
    selected_category_query_result = Database.executeThenFetchAll(
      'SELECT * FROM item_category where item_name=%s;',
      (name, )
    )

    # populate context with current item being updated
    form['data'] = {
      'name': item_query_result[0][0], 
      'description': item_query_result[0][1],
      'min_age': item_query_result[0][2],
      'max_age': item_query_result[0][3],
      'material': item_query_result[0][4],
      'category': [e[1] for e in selected_category_query_result]
    }

    return render(request, 'toys/item/update.html', {'form': form})
  else:
    form['data'] = copy.deepcopy(request.POST)
    post_data = _set_empty_field_to_null(dict(request.POST))

    # update in item table
    Database.executeThenCommit(
      'UPDATE ITEM SET name=%s, description=%s, min_age=%s, max_age=%s, material=%s \
          where name=%s;',
      (post_data["name"][0], post_data["description"][0], post_data["min_age"][0],
          post_data["max_age"][0], post_data["material"][0], name)
    )
    # update in item_category table
    previous_selected_category_query_result = Database.executeThenFetchAll(
      'SELECT category_name FROM item_category where item_name=%s;',
      (name, )
    )
    previous_selected_set = set([e[0] for e in previous_selected_category_query_result])
    new_selecteed_set = set(post_data['category'])
    # only insert and delete necessary item_category records
    for to_be_added in new_selecteed_set - previous_selected_set:
      Database.executeThenCommit(
        'INSERT INTO ITEM_CATEGORY VALUES(%s, %s);',
        (post_data["name"][0], to_be_added)
      )
    for to_be_deleted in previous_selected_set - new_selecteed_set:
      Database.executeThenCommit(
        'DELETE FROM ITEM_CATEGORY WHERE item_name=%s AND category_name=%s;',
        (post_data["name"][0], to_be_deleted)
      )
    
    messages.success(request, name + ' updated successfully')
    return HttpResponseRedirect(reverse('toys:item_index'))

@require_http_methods(["POST"])
@csrf_exempt
@roles_required('admin')
def item_delete(request, name):
  Database.executeThenCommit(
    'DELETE FROM ITEM WHERE name=%s',
    (name, )
  )
  messages.success(request, name + ' deleted successfully')
  return HttpResponseRedirect(reverse('toys:item_index'))

# goods
@roles_required("any")
def goods_index(request):
    database = Database
    res = database.executeThenFetchAll("select * from goods")
    query_category =database.executeThenFetchAll("select distinct category_name from item_category")
    if request.method =="POST":
        checked = request.POST.getlist('checked')
        request.session['checked'] = checked
        return render(request, 'toys/goods/index.html',
                      {'res': res, 'orders': checked, 'categories': query_category, 'role': request.session['role']})

    else :
        checked = request.session.get('checked')
        print(request.GET.get("category_filter"))
        print(request.GET.get("sort_by") == "" ,request.GET.get("category_filter") == "")
        print(request.GET.get("sort_by"), request.GET.get("category_filter"))

        if request.GET.get("sort_by") == None and request.GET.get("category_filter") == None:
            checked = request.session.get('checked')
            print(checked)
            return render(request, 'toys/goods/index.html',
                          {'res': res, 'orders': checked, 'categories': query_category, 'role': request.session['role']})

        if request.GET.get("sort_by") != "" and request.GET.get("category_filter") != "":
            if request.GET.get("sort_by") == 'item_name':
                res = database.executeThenFetchAll("select distinct * from goods g, item_category i where i.category_name=%s and g.item_name=i.item_name order by i.category_name,g.condition asc;",(request.GET.get("category_filter"),))
                return render(request, 'toys/goods/index.html',
                              {'res': res, 'orders': checked, 'categories': query_category,'role':request.session['role']})
            else:
                res = database.executeThenFetchAll("select distinct * from goods g, item_category i where i.category_name=%s and g.item_name=i.item_name order by i.category_name,g.condition asc;",(request.GET.get("category_filter"),))
                return render(request, 'toys/goods/index.html',
                              {'res': res, 'orders': checked, 'categories': query_category,'role':request.session['role']})


        elif request.GET.get("category_filter") != "" and request.GET.get("sort_by") == "":
            res = database.executeThenFetchAll("select distinct * from goods g,item_category i where i.category_name=%s and g.item_name=i.item_name;",(request.GET.get("category_filter"),))
            print(res)
            return render(request, 'toys/goods/index.html',
                          {'res': res, 'orders': checked, 'categories': query_category,
                           'role': request.session['role']})

        elif request.GET.get("sort_by") != "" and request.GET.get("category_filter") == "":
            print("I AM HERE")
            if request.GET.get("sort_by") == 'item_name':
                res = database.executeThenFetchAll("select distinct * from goods g order by g.item_name asc;")
                return render(request, 'toys/goods/index.html',
                              {'res': res, 'orders': checked, 'categories': query_category,'role':request.session['role']})
            else:
                res = database.executeThenFetchAll("select distinct * from goods g, item_category i where  g.item_name=i.item_name order by i.category_name,g.condition asc;")
                print(res)
                return render(request, 'toys/goods/index.html',
                              {'res': res, 'orders': checked, 'categories': query_category,'role':request.session['role']})
        else:
            checked = request.session.get('checked')
            print(checked)
            return render(request, 'toys/goods/index.html',
                          {'res': res, 'orders': checked, 'categories': query_category, 'role': request.session['role']})


@roles_required("any")
def goods_detail(request,goods_id):
  query_result = Database.executeThenFetchAll('select * from goods WHERE goods_id=%s;',(goods_id,))
  context = { "obj": query_result }
  return render(request, 'toys/goods/detail.html',context)

@roles_required("admin")
def goods_create(request):
    query_member = Database.executeThenFetchAll("select * from app_user a, member m where a.id_number = m.id_number;")
    query_item = Database.executeThenFetchAll("select name from item i")
    context={'items':query_item,
             'members':query_member
             }
    if request.method == "POST":
        id = request.POST.get("goods_id")
        item_name = request.POST.get("item_name")
        color = request.POST.get("color")
        photo = request.POST.get("photo")
        condition = request.POST.get("condition")
        usage = request.POST.get("usage")
        owner = request.POST.get("owner")
        bronze_rent = request.POST.get("bronze_rent")
        bronze_royalty = request.POST.get("bronze_royalty")
        silver_rent = request.POST.get("silver_rent")
        silver_royalty = request.POST.get("silver_royalty")
        gold_rent = request.POST.get("gold_rent")
        gold_royalty = request.POST.get("gold_royalty")
        Database.executeThenCommit(
            'insert into goods values (%s,%s,%s,%s,%s,%s,%s);',
            (id,item_name, color, photo, condition, usage, owner,)
        )

        Database.executeThenCommit(
            'insert into item_level_info values (%s,%s,%s,%s);',(id,'bronze',bronze_rent,bronze_royalty,)
        )

        Database.executeThenCommit(
            'insert into item_level_info values (%s,%s,%s,%s);',(id,'silver',silver_rent,silver_royalty,)
        )

        Database.executeThenCommit(
            'insert into item_level_info values (%s,%s,%s,%s);',(id,'gold',gold_rent,gold_royalty,)
        )
        print(id,item_name,color,photo,condition,usage,owner,bronze_rent,bronze_royalty,silver_rent,silver_royalty,gold_rent,gold_royalty)
        return HttpResponseRedirect(reverse('toys:goods_index'))


    return render(request,'toys/goods/create.html',context)

@roles_required("admin")
def goods_update(request,goods_id):
  query_result = Database.executeThenFetchAll('select * from goods,app_user WHERE goods_id=%s and renter_id_num != id_number;',(goods_id,))
  query_member = Database.executeThenFetchAll("select * from app_user a, member m where a.id_number = m.id_number;")
  pre_selected_item = Database.executeThenFetchAll("select name from item i,goods g where g.goods_id =%s and g.item_name = i.name",(goods_id,))
  query_item = Database.executeThenFetchAll("select name from item i")
  query_level=  Database.executeThenFetchAll("select * from item_level_info where goods_id=%s order by level_name",(goods_id,))
  renter = Database.executeThenFetchAll("select a.id_number, a.full_name from app_user a, goods g where goods_id=%s and g.renter_id_num = a.id_number;",(goods_id,))
  print(query_level)
  context = {"obj": query_result,
             "members": query_member,
             "renter":renter,
             "pre_selected_item":pre_selected_item,
             "items":query_item,
             "bronze_rent":query_level[0][2],
             "bronze_royalty":query_level[0][3],
             "silver_rent":query_level[2][2],
             "silver_royalty": query_level[2][3],
             "gold_rent": query_level[1][2],
             "gold_royalty":query_level[1][3],
             }
  if request.method == "POST":
      id = request.POST.get("goods_id")
      item_name = request.POST.get("item_name")
      color= request.POST.get("color")
      photo = request.POST.get("photo")
      condition =  request.POST.get("condition")
      usage = request.POST.get("usage")
      owner = request.POST.get("owner")
      bronze_rent= request.POST.get("bronze_rent"),
      bronze_royalty=  request.POST.get("bronze_royalty"),
      silver_rent=  request.POST.get("silver_rent"),
      silver_royalty=  request.POST.get("silver_royalty"),
      gold_rent=  request.POST.get("gold_rent"),
      gold_royalty=  request.POST.get("gold_royalty"),
      print(owner)
      Database.executeThenCommit('update goods set item_name=%s, color = %s, photo_url= %s, condition = %s, usage_duration = %s,renter_id_num=%s where goods_id = %s;',(item_name,color,photo,condition,usage,owner,id,))
      Database.executeThenCommit('update item_level_info set rent_price=%s, royalty_portion=%s where goods_id=%s and level_name=%s',(bronze_rent,bronze_royalty,goods_id,"bronze"))
      Database.executeThenCommit(
          'update item_level_info set rent_price=%s, royalty_portion=%s where goods_id=%s and level_name=%s',
          (silver_rent, silver_royalty, goods_id, "silver"))
      Database.executeThenCommit(
          'update item_level_info set rent_price=%s, royalty_portion=%s where goods_id=%s and level_name=%s',
          (gold_rent, gold_royalty, goods_id, "gold"))

      return HttpResponseRedirect(reverse('toys:goods_index'))

  return render(request,'toys/goods/update.html',context)

@roles_required("admin")
def goods_delete(request,goods_id):
  print("DELETED ",goods_id)
  Database.executeThenCommit('delete from goods where goods_id =%s;',(goods_id,))
  return HttpResponseRedirect(reverse('toys:goods_index'))


# chat
def chat_admin(request):
    return render(request, 'toys/chat/admin.html')


def chat_member(request):
    return render(request, 'toys/chat/member.html')


# level
@roles_required('admin')
@require_http_methods(["GET"])
def level_index(request):
  query_result = Database.executeThenFetchAll(
      'SELECT * FROM MEMBERSHIP_LEVEL;'
    )
  data = {'membership_level_list' : query_result}
  return render(request, 'toys/level/index.html', data)


@roles_required('admin')
@require_http_methods(["GET", "POST"])
def level_create(request):
  if request.method == 'GET':
    return render(request, 'toys/level/create.html')
  if request.method == 'POST':
    post_data = _set_empty_field_to_null(dict(request.POST))
    if post_data['level_name'][0] == None:
      messages.error(request, 'level name must be filled')
      return HttpResponseRedirect(reverse('toys:level_create'))
    if _level_name_exist(post_data["level_name"][0]):
      messages.error(request, 'level name already exist')
      return HttpResponseRedirect(reverse('toys:level_create'))
    Database.executeThenCommit(
      'INSERT INTO MEMBERSHIP_LEVEL VALUES(%s, %s, %s);',
      (post_data['level_name'][0],post_data['minimum_points'][0],post_data['description'][0])
    )
    return HttpResponseRedirect(reverse('toys:level_index'))

@roles_required('admin')
@require_http_methods(["GET", "POST"])
def level_update(request,primary_key):
  if request.method == 'GET':
    query_result = Database.executeThenFetchAll(
      'SELECT * FROM MEMBERSHIP_LEVEL WHERE LEVEL_NAME = %s;',
      (primary_key,)
    )

    print(query_result)
    data = {'membership_level' : query_result}
    return render(request, 'toys/level/update.html', data)
  if request.method == 'POST':
    post_data = _set_empty_field_to_null(dict(request.POST))
    Database.executeThenCommit(
      'UPDATE MEMBERSHIP_LEVEL SET MINIMUM_POINTS=%s,DESCRIPTION=%s WHERE LEVEL_NAME=%s;',
      (post_data['minimum_points'][0],post_data['description'][0],primary_key)
    )
    return HttpResponseRedirect(reverse('toys:level_index'))

@roles_required('admin')
def level_delete(request,primary_key):
  Database.executeThenCommit(
    'DELETE FROM MEMBERSHIP_LEVEL WHERE LEVEL_NAME=%s',
    (primary_key,)
  )
  messages.success(request, 'level {} has been successfully deleted!'.format(primary_key))
  return HttpResponseRedirect(reverse('toys:level_index'))

def _level_name_exist(level_name):
  # return true if email already registered
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM MEMBERSHIP_LEVEL WHERE LEVEL_NAME=%s;', 
    (level_name, )
  )

  if query_result:
    return True
  return False

def _set_empty_field_to_null(post_data):
  # returns different copy of post_data with value ['']
  # replaced with [None]
  post_data_copy = copy.deepcopy(post_data)
  for key, value in post_data_copy.items():
    if not value:
      post_data_copy[key] = [None]
    elif type(value) == list:
      for index, element in enumerate(value):
        # check if any element in list have falsey value
        # if so, change to None
        if not element:
          post_data_copy[key][index] = None
          break

  return post_data_copy

# order
def order_index(request):
    user_data = request.session['id']
    query_member = Database.executeThenFetchAll('SELECT * FROM app_order WHERE ordered_id_num=%s;', (user_data,))
    query_admin = Database.executeThenFetchAll('SELECT * FROM app_order')

    if len(query_member) != 0:
        result = {
            "order_member": query_member,
            "order_admin": query_admin
        }
    else:
        result = {
            "order_admin": query_admin
        }

    return render(request, 'toys/order/index.html', result)


@require_http_methods(["GET", "POST"])
def order_create(request):
    if request.method == 'GET':
        query_goods = Database.executeThenFetchAll('SELECT * FROM goods WHERE condition=%s;', ("available",))
        query_member = Database.executeThenFetchAll('SELECT * FROM member NATURAL JOIN app_user')

        result = {
            "goods": query_goods,
            "members": query_member
        }

        return render(request, 'toys/order/create.html', result)

    if request.method == 'POST':
        post_data = _set_empty_field_to_null(dict(request.POST))

        if not _required_fields_is_filled(post_data, ['rent_dur']):
            messages.error(request, "The rent duration must be filled")
            return HttpResponseRedirect(reverse('toys:order_create'))
        else:
            messages.success(request, "You did it")
            return HttpResponseRedirect(reverse('toys:order_index'))


@require_http_methods(["GET"])
def order_update(request, order):
    if request.method == 'GET':
        query_result = Database.executeThenFetchAll(
            'SELECT DISTINCT status FROM app_order EXCEPT SELECT status FROM app_order WHERE order_id=%s;', (order,))

        get_status = Database.executeThenFetchAll('SELECT status FROM app_order WHERE order_id=%s;', (order,))

        get_ordered_goods = Database.executeThenFetchAll('SELECT * FROM ordered_goods where order_id=%s;', (order,))

        if len(get_ordered_goods) != 0:
            query_good = Database.executeThenFetchAll('SELECT * FROM goods WHERE goods_id=%s;',
                                                      (get_ordered_goods[0][2],))
            query_goods = Database.executeThenFetchAll('SELECT * FROM goods WHERE condition=%s;', ("available",))

            result = {
                "status": query_result,
                "stats": get_status,
                "good": query_good,
                "goods": query_goods,
                "rent": get_ordered_goods
            }
            return render(request, 'toys/order/update.html', result)

        else:
            messages.error(request, "! Sorry! the goods for this order doesnt exists in our database")
            return HttpResponseRedirect(reverse('toys:order_index'))


@require_http_methods(["POST"])
def order_make_update(request):
    if request.method == 'POST':
        post_data = _set_empty_field_to_null(dict(request.POST))

        if not _required_fields_is_filled(post_data, ['rent_dur']):
            messages.error(request, "The rent duration must be filled")
            return HttpResponseRedirect(reverse('toys:order_create'))
        else:
            Database.executeThenCommit('UPDATE ordered_goods SET rent_duration=%s, goods_id=%s WHERE order_id=%s;',
                                       (post_data['rent_dur'][0], post_data["goods"][0][2:12], post_data["order_id"][0])
                                       )
            if post_data["status"][0] != "status":
                Database.executeThenCommit('UPDATE app_order SET status=%s WHERE order_id=%s;',
                                           (post_data["status"][0],
                                            post_data["order_id"][0]))
            messages.success(request, "You have successfully updated the data!")

            return HttpResponseRedirect(reverse('toys:order_index'))


@require_http_methods(["GET"])
def order_delete(request, order):
    if request.method == 'GET':
        Database.executeThenCommit('DELETE FROM app_order WHERE order_id=%s;', (order,))
        messages.success(request, "You have deleted the order!")

        return HttpResponseRedirect(reverse('toys:order_index'))


# delivery
def delivery_index(request):
  return render(request, 'toys/delivery/index.html')

@roles_required('any')
@require_http_methods(["GET", "POST"])
def delivery_create(request):
  if request.method == 'GET':
    goods_list = Database.executeThenFetchAll(
        'SELECT order_id FROM APP_ORDER EXCEPT SELECT order_id FROM DELIVERY;'
    )
    if request.session['role'] == 'member':
      address_list = Database.executeThenFetchAll(
        'SELECT name FROM ADDRESS WHERE member_id_num = %s;',
        (request.session['id'],)
      )
    if request.session['role'] == 'admin':
      address_list = Database.executeThenFetchAll(
        'SELECT name FROM ADDRESS;'
      )
    data = {
      'goods_list' : goods_list,
      'address_list' : address_list
    }
    return render(request, 'toys/delivery/create.html',data)
  if request.method == 'POST':
    post_data = _set_empty_field_to_null(dict(request.POST))
    if(not delivery_isvalid(post_data)):
      messages.error(request, 'data is not valid!')
      return HttpResponseRedirect(reverse('toys:delivery_create'))
    charge = Database.executeThenFetchAll(
      'SELECT charge FROM APP_ORDER WHERE order_id=%s;', 
      (post_data['ordered_goods'][0], )
      )[0][0]
    member_id = Database.executeThenFetchAll(
      'SELECT member_id_num FROM ADDRESS WHERE name=%s;', 
      (post_data['destination_address'][0], )
      )[0][0]
    Database.executeThenCommit(
      'INSERT INTO DELIVERY VALUES(%s, %s, %s, %s, %s, %s, %s);',
      (get_receipt_num_key(),post_data['ordered_goods'][0],post_data['method'][0],charge,post_data['date'][0],member_id,post_data['destination_address'][0])
    )
    messages.success(request, 'delivery form successfully stored!')
    return HttpResponseRedirect(reverse('toys:delivery_index'))
def get_receipt_num_key():
  receipt_num = Database.executeThenFetchAll(
    'select receipt_num from delivery where char_length(receipt_num) = (select max(char_length(receipt_num)) from delivery) order by receipt_num desc limit 1;'
  )
  return str(int(receipt_num[0][0])+1)
def delivery_isvalid(post_data):
  if post_data['method'] == None and post_data['date'] == None:
    return False
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM APP_ORDER WHERE order_id=%s;', 
    (post_data['ordered_goods'][0], )
  )
  if query_result:
    return True
  return False

@roles_required('any')
@require_http_methods(["GET", "POST"])
def delivery_update(request,primary_key):
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM DELIVERY WHERE receipt_num = %s;',
    (primary_key,))
  if not query_result:
    messages.error(request, 'receipt number {} is not stored in the data base!'.format(primary_key))
    return HttpResponseRedirect(reverse('toys:delivery_index'))
  if request.method == 'GET':
    goods_list = Database.executeThenFetchAll(
        'SELECT order_id FROM APP_ORDER EXCEPT SELECT order_id FROM DELIVERY;')
    if request.session['role'] == 'member':
      address_list = Database.executeThenFetchAll(
        'SELECT name FROM ADDRESS WHERE member_id_num = %s;',
        (request.session['id'],))
    if request.session['role'] == 'admin':
      address_list = Database.executeThenFetchAll('SELECT name FROM ADDRESS;')
    data = {
      "delivery_list" : query_result,
      'goods_list' : goods_list,
      'address_list' : address_list
      }
    return render(request, 'toys/delivery/update.html',data)
  if request.method == 'POST':
    if request.session['role'] == 'member':
      # Check if delivery id belongs to the member id
      query_result =Database.executeThenFetchAll(
        'SELECT * FROM DELIVERY WHERE receipt_num=%s and member_id_num=%s;',
        (primary_key,request.session['id']))
      if not query_result:
        messages.error(request, 'data is not valid!')
        return HttpResponseRedirect(reverse('toys:delivery_update'))
    post_data = _set_empty_field_to_null(dict(request.POST))
    if(not delivery_isvalid(post_data)):
      messages.error(request, 'data is not valid!')
      return HttpResponseRedirect(reverse('toys:delivery_update'))
    member_id = Database.executeThenFetchAll(
      'SELECT member_id_num FROM ADDRESS WHERE name=%s;', 
      (post_data['destination_address'][0], )
      )[0][0]
    Database.executeThenCommit(
      'UPDATE DELIVERY SET ORDER_ID=%s,MEMBER_ADDRESS_NAME=%s,MEMBER_ID_NUM=%s,DATE=%s,METHOD=%s WHERE RECEIPT_NUM=%s;',
      (post_data['ordered_goods'][0],post_data['destination_address'][0],member_id,post_data['date'][0],post_data['method'][0],primary_key)
    )
    messages.success(request, 'successfully updated!')
    return HttpResponseRedirect(reverse('toys:delivery_index'))

@roles_required('any')
def delivery_delete(request,primary_key):
  if request.session['role'] == 'member':
    # Check if delivery id belongs to the member id
    query_result =Database.executeThenFetchAll(
      'SELECT * FROM DELIVERY WHERE receipt_num=%s and member_id_num=%s;',
      (primary_key,request.session['id']))
    if not query_result:
      messages.error(request, 'data is not valid!')
      return HttpResponseRedirect(reverse('toys:delivery_index'))
  Database.executeThenCommit(
    'DELETE FROM DELIVERY WHERE receipt_num=%s',
    (primary_key,)
  )
  messages.success(request, 'level {} has been successfully deleted!'.format(primary_key))
  return HttpResponseRedirect(reverse('toys:delivery_index'))

# goods review
def goods_review_index(request):
    return render(request, 'toys/goods/review/index.html')


def goods_review_create(request):
    return render(request, 'toys/goods/review/create.html')


def goods_review_update(request):
    return render(request, 'toys/goods/review/update.html')


def _set_empty_field_to_null(post_data):
    # returns different copy of post_data with value ['']
    # replaced with [None]
    post_data_copy = copy.deepcopy(post_data)
    for key, value in post_data_copy.items():
        if not value:
            post_data_copy[key] = [None]
        elif type(value) == list:
            for index, element in enumerate(value):
                # check if any element in list have false value
                # if so, change to None
                if not element:
                    post_data_copy[key][index] = None
                    break

    return post_data_copy


def _required_fields_is_filled(post_data, required_fields):
    # return true if all fields specified in list of string
    # required_fields is not null in post_data
    for key, value in post_data.items():
        if key in required_fields:
            if not value:
                # check value
                return False
            elif type(value) == list:
                # check every element in value
                for element in value:
                    if not element:
                        return False
    return True
