from django.urls import path, re_path

from . import views

app_name = 'toys'

urlpatterns = [
  # item
  path('item/', views.item_index, name="item_index"),
  path('item/category/<str:category>', views.item_index, name="item_index_category"),
  path('item/sort/<int:sort>', views.item_index, name="item_index_sort"),
  path('item/create', views.item_create, name="item_create"),
  path('item/<str:name>/update', views.item_update, name="item_update"),
  path('item/<str:name>/delete', views.item_delete, name="item_delete"),

  # goods
  path('goods', views.goods_index, name="goods_index"),
  path('goods/detail/<str:goods_id>', views.goods_detail, name="goods_detail"),
  path('goods/create', views.goods_create, name="goods_create"),
  path('goods/delete/<str:goods_id>', views.goods_delete, name="goods_delete"),
  path('goods/update/<str:goods_id>', views.goods_update, name="goods_update"),
  
  # level
  path('level', views.level_index, name="level_index"),
  path('level/create', views.level_create, name="level_create"),
  path('level/update/<str:primary_key>/', views.level_update, name="level_update"),
  path('level/delete/<str:primary_key>/', views.level_delete, name="level_delete"),

  # order
  path('order', views.order_index, name="order_index"),
  path('order/create', views.order_create, name="order_create"),
  path('order/update/<str:order>', views.order_update, name="order_update"),
  path('order/update', views.order_make_update, name="order_make_update"),
  path('order/delete/<str:order>', views.order_delete, name="order_delete"),

  # chat
  path('chat/admin', views.chat_admin, name="chat_admin"),
  path('chat/member', views.chat_member, name="chat_member"),

  # delivery
  path('delivery', views.delivery_index, name="delivery_index"),
  path('delivery/create', views.delivery_create, name="delivery_create"),
  path('delivery/update/<str:primary_key>/', views.delivery_update, name="delivery_update"),
  path('delivery/delete/<str:primary_key>/', views.delivery_delete, name="delivery_delete"),

  # goods review
  path('goods/review', views.goods_review_index, name="goods_review_index"),
  path('goods/review/create', views.goods_review_create, name="goods_review_create"),
  path('goods/review/update', views.goods_review_update, name="goods_review_update"),

]
