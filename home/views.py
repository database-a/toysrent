from django.shortcuts import render

def index(request):
  if request.session.get('role', None) == 'admin':
    return render(request, 'admin/admin_home.html', \
        {"username": request.session['username'], "userid": request.session['id']})
  else:
    return render(request, 'home/index.html')