import os
import psycopg2

class Database:
  '''
  a singleton connection object to heroku's postgresql database
  '''
  __instance = None

  @staticmethod 
  def getConnectionInstance():
    if Database.__instance == None:
      Database()
    return Database.__instance
  
  @staticmethod
  def executeThenFetchAll(query_in_string, query_params=None):
    '''
    returns list of tuples representing objects queried or 
    empty list if not found
    '''
    cursor = Database.getConnectionInstance().cursor()
    cursor.execute("set search_path to toysrent;")
    if query_params:
      cursor.execute(query_in_string, query_params)
    else:
      cursor.execute(query_in_string)
    result = cursor.fetchall()
    cursor.close()
    
    return result

  @staticmethod
  def executeThenCommit(query_in_string, query_params=None):
    '''
    executes query and saves changes to database
    '''
    cursor = Database.getConnectionInstance().cursor()
    cursor.execute("set search_path to toysrent;")
    if query_params:
      cursor.execute(query_in_string, query_params)
    else:
      cursor.execute(query_in_string)
    cursor.close()

  def __init__(self):
    '''
    do not use return value of this method
    '''
    if Database.__instance != None:
      raise Exception("attempt to instantiate another singleton Database")
    else:
      DATABASE_URL = os.environ['DATABASE_URL']
      conn = psycopg2.connect(DATABASE_URL, sslmode='require')
      conn.set_session(autocommit=True)
      
      print('connection to heroku postgre established!')
      Database.__instance = conn