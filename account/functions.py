from functools import wraps

from django.shortcuts import reverse
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.contrib import messages

from toysrent.database import Database

# use decorator at bottom for authenticating view functions

def is_logged_in(request):
  return True if request.session.get('id', None) else False

def is_admin_logged_in(request):
  return True if request.session.get('role', None) == 'admin' else False
  
def is_member_logged_in(request):
  return True if request.session.get('role', None) == 'member' else False

def roles_required(required_role):
  '''
  decorator to check authentication, takes in a string

  required_role can be any of the following: 
  "any" - either member or admin can access, if not logged in, redirect to login page
  "member" - only member can access page, if not member then return 403 forbidden
  "admin" - only admin can access page, if not admin then return 403 forbidden

  example usage:
  @roles_required("any")
  def my_view(request):
    # do something
  '''
  def decorator(func):
    @wraps(func)
    def inner(request, *args, **kwargs):
      if not(type(required_role)==str and required_role in ['any', 'member', 'admin']):
        raise ValueError("argument must be string with values: any, member or admin")
      if not request.session.get('id', None):
        messages.error(request, 'page requires login')
        return HttpResponseRedirect(reverse('account:login'))
      if required_role in ['member', 'admin']:
        if request.session.get('role', None) != required_role:
          return HttpResponseForbidden('current user does not have access')
      return func(request, *args, **kwargs)
    return inner
  return decorator
