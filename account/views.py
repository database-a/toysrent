from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.http import require_http_methods
from django.contrib import messages

from account.functions import roles_required
from toysrent.database import Database

import copy

@require_http_methods(["GET", "POST"])
def login(request):
  if request.method == 'GET':
    return render(request, 'account/login.html')

  if request.method == 'POST':
    # include previous form data and errors
    form = { 'data': copy.deepcopy(request.POST), 'error': {} } 
    del form['data']['csrfmiddlewaretoken'] 

    post_data = dict(request.POST)
    # check if user exist
    query_result = Database.executeThenFetchAll(
      'SELECT * FROM app_user WHERE id_number=%s AND email=%s;', 
      (post_data["id_number"][0], post_data["email"][0])
    )

    if query_result:
      _set_session_for(post_data["id_number"][0], request)
      if request.session['role'] == 'admin':
        messages.success(request, 'Logged in as admin')
        return HttpResponseRedirect(reverse('toys:order_index'))
      else:
        messages.success(request, 'Logged in as member')
        return HttpResponseRedirect(reverse('toys:goods_index'))
    else:
      form['error']['id_number'] = 'invalid ktp number or email'
      form['error']['email'] = 'invalid ktp number or email'

      return render(request, 'account/login.html', {'form': form})

@require_http_methods(["POST"])
def logout(request):
  try:
    # clear session info
    del request.session['id']
    del request.session['username']
    del request.session['role']
  except KeyError:
    pass
  messages.success(request, 'Logged out')
  return HttpResponseRedirect(reverse('account:login'))

@require_http_methods(["GET"])
def register(request):
  # if not specified, default page rendered
  # will be member registration
  return HttpResponseRedirect(reverse('account:register_member'))

@require_http_methods(["GET", "POST"])
def register_member(request):
  if request.method == 'GET':
    return render(request, 'account/register-member.html')
  if request.method == 'POST':
    # include previous form data and errors
    form = { 'data': copy.deepcopy(request.POST), 'error': {} }
    del form['data']['csrfmiddlewaretoken'] 

    post_data = _set_empty_field_to_null(dict(request.POST))

    # chcek required fields
    if not _required_fields_is_filled(post_data, ['id_number']):
      form['error']['id_number'] = 'KTP number required'
    if not _required_fields_is_filled(post_data, ['full_name']):
      form['error']['full_name'] = 'Full Name required'
    if not _required_fields_is_filled(post_data, ['email']):
      form['error']['email'] = 'Email required'
    if form['error']:
      return render(request, 'account/register-member.html', {'form':form})

    # do validations
    if _ktp_num_exist(post_data["id_number"][0]):
      form['error']['id_number'] = 'KTP number already registered'
    if _email_exist(post_data["email"][0]):
      form['error']['email'] = 'email already registered'
    if form['error']:
      return render(request, 'account/register-member.html', {'form':form})

    addresses = zip(*[iter(post_data["address[]"])]*5)
    address_names = [name for name, _, _,_ ,_ in addresses]
    
    # check all address field not null
    if None in post_data["address[]"]:
      form['error']['address'] = 'All address fields required'
      return render(request, 'account/register-member.html', {'form':form})

    # check unique address name
    if len(address_names) != len(set(address_names)):
      form['error']['address'] = 'Address name must be unique'
      return render(request, 'account/register-member.html', {'form':form})

    # reset iterator
    addresses = zip(*[iter(post_data["address[]"])]*5)
    for _, _, address_number, _, _ in addresses:
      if address_number != None and not address_number.isdigit():
        form['error']['address'] = 'Address number must be a number'
        return render(request, 'account/register-member.html', {'form':form})

    # create app_user and admin record in database
    Database.executeThenCommit(
      'INSERT INTO APP_USER VALUES(%s, %s, %s, %s, %s);',
      (post_data["id_number"][0], post_data["full_name"][0], post_data["email"][0],
          post_data["birth_date"][0], post_data["phone_num"][0])
    )
    Database.executeThenCommit(
      'INSERT INTO MEMBER VALUES(%s, %s, %s);',
      (post_data["id_number"][0], 0, 'bronze')
    )

    # reset iterator
    addresses = zip(*[iter(post_data["address[]"])]*5)
    # if have multiple address fields, field data will be concatenated to one list
    # hence iterate 5 items at a time throught address[] list
    for address_name, address_road, address_number, address_city, address_postcode \
        in addresses:
      # dont insert if all 5 input are null
      if address_name or address_road or address_number or address_city or address_postcode: 
        Database.executeThenCommit(
          'INSERT INTO ADDRESS VALUES(%s, %s, %s, %s, %s, %s);',
          (post_data["id_number"][0], address_name, address_road, 
              int(address_number) if address_number != None else address_number, 
              address_city, address_postcode)
        )

    _set_session_for(post_data["id_number"][0], request)
    messages.success(request, 'member account created successfully')
    return HttpResponseRedirect(reverse('toys:goods_index'))

@require_http_methods(["GET", "POST"])
def register_admin(request):
  if request.method == 'GET':
    return render(request, 'account/register-admin.html')
  if request.method == 'POST':
    # include previous form data and errors
    form = { 'data': copy.deepcopy(request.POST), 'error': {} }
    del form['data']['csrfmiddlewaretoken'] 

    post_data = _set_empty_field_to_null(dict(request.POST))

    # chcek required fields
    if not _required_fields_is_filled(post_data, ['id_number']):
      form['error']['id_number'] = 'KTP number required'
    if not _required_fields_is_filled(post_data, ['full_name']):
      form['error']['full_name'] = 'Full Name required'
    if not _required_fields_is_filled(post_data, ['email']):
      form['error']['email'] = 'Email required'
    if form['error']:
      return render(request, 'account/register-admin.html', {'form':form})

    # do validations
    if _ktp_num_exist(post_data["id_number"][0]):
      form['error']['id_number'] = 'KTP number already registered'
    if _email_exist(post_data["email"][0]):
      form['error']['email'] = 'email already registered'
    if form['error']:
      return render(request, 'account/register-admin.html', {'form':form})

    # create app_user and admin record in database
    Database.executeThenCommit(
      'INSERT INTO APP_USER VALUES(%s, %s, %s, %s, %s);',
      (post_data["id_number"][0], post_data["full_name"][0], post_data["email"][0],
          post_data["birth_date"][0], post_data["phone_num"][0])
    )
    Database.executeThenCommit(
      'INSERT INTO ADMIN VALUES(%s);',
      (post_data["id_number"][0], )
    )

    messages.success(request, 'admin account created successfully')
    _set_session_for(post_data["id_number"][0], request)
    return HttpResponseRedirect(reverse('toys:order_index'))

@require_http_methods(["GET"])
@roles_required('any')
def profile(request):
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM app_user WHERE id_number=%s;', 
    (request.session['id'], )
  )
  data = {
    'id_number': query_result[0][0], 
    'full_name': query_result[0][1], 
    'email': query_result[0][2], 
    'birth_date': query_result[0][3], 
    'phone_num': query_result[0][4], 
  }
  if request.session['role'] == 'member':
    member_query_result = Database.executeThenFetchAll(
      'SELECT * FROM member WHERE id_number=%s;', 
      (request.session['id'], )
    )
    data['point'] = member_query_result[0][1]
    data['level'] = member_query_result[0][2]
  print(data)
  return render(request, 'account/profile.html', data)

# below are helper functions

def _app_user_is_admin(app_user_ktp_num):
  # returns true if ktp number in argument is admin's
  # meant to be used only in this file
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM admin WHERE id_number=%s;', 
    (app_user_ktp_num, )
  )

  if query_result:
    return True
  return False

def _ktp_num_exist(ktp_num):
  # return true if ktp number already registered
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM app_user WHERE id_number=%s;', 
    (ktp_num, )
  )

  if query_result:
    return True
  return False

def _email_exist(email):
  # return true if email already registered
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM app_user WHERE email=%s;', 
    (email, )
  )

  if query_result:
    return True
  return False

def _set_empty_field_to_null(post_data):
  # returns different copy of post_data with value ['']
  # replaced with [None]
  post_data_copy = copy.deepcopy(post_data)
  for key, value in post_data_copy.items():
    if not value:
      post_data_copy[key] = [None]
    elif type(value) == list:
      for index, element in enumerate(value):
        # check if any element in list have falsey value
        # if so, change to None
        if not element:
          post_data_copy[key][index] = None

  return post_data_copy

def _required_fields_is_filled(post_data, required_fields):
  # return true if all fields specified in list of string 
  # required_fields is not null in post_data
  for key, value in post_data.items():
    if key in required_fields:
      if not value:
        # check value
        return False
      elif type(value) == list:
        # check every element in value
        for element in value:
          if not element:
            return False
  return True

def _set_session_for(ktp_num, request):
  query_result = Database.executeThenFetchAll(
    'SELECT * FROM app_user WHERE id_number=%s;', 
    (ktp_num, )
  )

  request.session['id'] = query_result[0][0]
  request.session['username'] = query_result[0][1]
  if _app_user_is_admin(query_result[0][0]):
    request.session['role'] = 'admin'
  else:
    request.session['role'] = 'member'