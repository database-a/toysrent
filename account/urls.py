from django.urls import path

from . import views

app_name = 'account'

urlpatterns = [
  path('login', views.login, name="login"),
  path('logout', views.logout, name="logout"),
  path('register', views.register, name="register"), 
  path('register/member', views.register_member, name="register_member"), 
  path('register/admin', views.register_admin, name="register_admin"), 
  path('profile', views.profile, name="profile"), 
]